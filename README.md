<p align="center">
    <img src="https://raw.githubusercontent.com/atvpc/atvpc.com/master/themes/atvpc-bootstrap/img/logo.png" alt="ATV Parts Connection Logo">
</p>
<p align="center">
    <a href="https://github.com/atvpc/atvpc.com/blob/master/LICENSE">
        <img src="https://img.shields.io/github/license/atvpc/atvpc.com.svg" alt="License Badge">
    </a>
    <a href="https://github.com/atvpc/atvpc.com/releases">
        <img src="https://img.shields.io/github/tag/atvpc/atvpc.com.svg" alt="Current Version">
    </a>
    <a href="https://www.codacy.com/app/timothykeith/atvpc.com">
        <img src="https://api.codacy.com/project/badge/Grade/c18d8f710dc744fb940f1124b0f9378e" alt="Codacy Badge">
    </a>
</p>

Source code for ATV Parts Connection's [website](http://atvpc.com). We use [Pico](https://github.com/picocms/Pico) for a CMS, [Yarn](https://yarnpkg.com/en/) for dependancy management, and a [variety of other](https://github.com/atvpc/atvpc.com/blob/bootstrap/humans.txt) excellent open-source technologies.

## Feedback

- **General business inquiries should be directed to:**  
  :email: **[sales@atvpc.com](mailto:sales@atvpc.com)**

- Bug reports or issues can be submitted to GitHub's Issue Tracker:  
  :beetle: [atvpc.com/issues](https://github.com/atvpc/atvpc.com/issues)

- For other technical concerns with the website, its code, or this git repo:  
  :email: [timothy@atvpc.com](mailto:timothy@atvpc.com)  
  :key: [28DC31E49D496D07861334417982B15239458B9C](https://gist.github.com/keithieopia/9dd6d4197f76c244e2e0daa4ebcd5c15)


## Copyright & License
Copyright &copy; 2014 &ndash; 2018 CV Restoration, LLC d. b. a. ATV Parts Connection, except where otherwise noted.

*ATV Parts Connection, Monster Performance Parts, the Monster Axle logo, and ATV Parts Connection graphics are the servicemarks, trademarks, or registered trademarks owned by ATV Parts Connection. All other servicemarks and trademarks are the property of their respective owner.*

---

Source code is licensed under the [MIT License](https://github.com/keithieopia/atvpc.com/blob/master/LICENSE).

*This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.*
